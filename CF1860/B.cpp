#include <iostream>
using namespace std;

#define lli long long int

void solve() {
  lli m, k, a1, ak;
  cin >> m >> k >> a1 >> ak;

  lli regularFirstTypeCoinValue = a1 * 1;
  lli regularSecondTypeCoinValue = ak * k;

  if (regularFirstTypeCoinValue + regularSecondTypeCoinValue >= m) {
    cout << 0 << endl;
  } else {
    lli extraAmount =
        m - (regularFirstTypeCoinValue + regularSecondTypeCoinValue);
    if (extraAmount % k == 0) {
      cout << extraAmount / k << endl;
    } else {
      if (((extraAmount / k) + (extraAmount % k)) <
          regularSecondTypeCoinValue) {
        cout << (extraAmount / k) + (extraAmount % k) - 1 << endl;
      } else {
        cout << (extraAmount / k) + (extraAmount % k) << endl;
      }
    }
  }
}

int main() {
  int t;
  cin >> t;
  while (t--) {
    solve();
  }
}
