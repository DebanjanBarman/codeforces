#include <iostream>
#include <unordered_set>

using namespace std;

int main() {
  char str[1000] = {};
  unordered_set<char> set;

  cin.getline(str, 1000, '\n');

  //  cout << str << endl;

  for (char s : str) {
    if (s != '{' && s != '}' && s != ' ' && s != ',' && s != '\0') {
      set.insert(s);
    }
  }
  cout << set.size() << endl;
}