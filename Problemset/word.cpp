#include <iostream>

using namespace std;

int main() {
  string str;
  int noOfUppercaseLetter = 0, noOfLowercaseLetter = 0;

  cin >> str;

  for (int i = 0; i < str.length(); i++) {
    if ((int)(str[i]) >= 65 && (int)(str[i]) <= 90) {
      noOfUppercaseLetter++;
    } else {
      noOfLowercaseLetter++;
    }
  }

  if (noOfUppercaseLetter > noOfLowercaseLetter) {
    char upperString[101];

    for (int i = 0; i < str.length(); i++) {
      upperString[i] = toupper(str[i]);
    }

    for (int i = 0; i < str.length(); i++) {
      cout << upperString[i];
    }

  } else {
    char lowerString[101];
    for (int i = 0; i < str.length(); i++) {
      lowerString[i] = tolower(str[i]);
    }
    for (int i = 0; i < str.length(); i++) {
      cout << lowerString[i];
    }
  }

  return 0;
}
