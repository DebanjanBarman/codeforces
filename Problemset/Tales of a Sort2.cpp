#include <iostream>
using namespace std;
int main() {
  int n;
  cin >> n;

  while (n--) {
    int arraySize;
    int currentMax = 0;
    int max = 0;

    cin >> arraySize;

    int array[arraySize];
    for (int i = 0; i < arraySize; ++i) {
      cin >> array[i];
    }

    for (int i = 0; i < arraySize - 1; ++i) {
      if (array[i] > array[i + 1]) {
        currentMax = array[i];
      }
      if (i == 0) {
        max = currentMax;
      }
      if (currentMax > max) {
        max = currentMax;
      }
    }
    cout << max << endl;
  }
}