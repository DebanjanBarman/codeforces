//
// Created by debanjan on 8/17/23.
//
#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;

  while (n--) {
    long long int a, b;
    cin >> a;
    cin >> b;

    if (a % b == 0) {
      cout << 0 << endl;
    } else {
      cout << b - (a % b) << endl;
    }
  }
}