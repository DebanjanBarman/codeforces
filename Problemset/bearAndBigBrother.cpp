//
// Created by debanjan on 8/11/23.
//
#include <iostream>
using namespace std;

int main() {
  int a, b = {};
  int noOfYear = 0;

  cin >> a >> b;

  while (a <= b) {
    a *= 3;
    b *= 2;
    noOfYear++;
  }
  cout << noOfYear << endl;
  return 0;
}