//
// Created by debanjan on 8/16/23.
//
#include <iostream>
#include <vector>

using namespace std;

void solve() {
  char str[8][8];

  for (int i = 0; i < 8; ++i) {
    for (int j = 0; j < 8; ++j) {
      cin >> str[i][j];
      if (str[i][j] != '.') {
        cout << str[i][j];
      } else {
        continue;
      }
    }
  }
  cout << endl;
}
int main() {
  int n;

  cin >> n;

  while (n--) {
    solve();
  }
}