//
// Created by debanjan on 8/22/23.
//
#include <iostream>
using namespace std;

#define lli long long int

int main() {
  int n;
  cin >> n;
  while (n--) {
    lli candies;
    cin >> candies;

    if (candies <= 2) {
      cout << 0 << endl;
      continue;
    }
    if (candies % 2 == 0) {
      cout << (lli)(candies / 2) - 1 << endl;
    } else {
      cout << (lli)(candies / 2) << endl;
    }
  }
}