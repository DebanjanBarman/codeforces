#include <bits/stdc++.h>
using namespace std;

void solve();

int main() {
  int t;
  cin >> t;
  while (t--) {
    solve();
  }
}

void solve() {
  int arrayLength;
  cin >> arrayLength;

  vector<int> array(arrayLength);
  for (int i = 0; i < arrayLength; ++i) {
    cin >> array[i];
  }

  sort(array.begin(), array.end());

  int B[100] = {}, C[100] = {};
  int lenOfB = 0, lenOfC = 0;

  B[lenOfB++] = array[arrayLength - 1];

  for (int i = 1; i < arrayLength; ++i) {
    if (array[i] % B[lenOfB - 1] == 0) {
      C[lenOfC++] = array[i];
    } else {
      B[lenOfB++] = array[i];
    }
  }

  if (lenOfC == 0) {
    cout << -1 << endl;
    return;
  }

  cout << lenOfB << " " << lenOfC << endl;

  for (int i = 0; i < lenOfB; ++i) {
    cout << B[i] << " ";
  }
  cout << endl;

  for (int i = 0; i < lenOfC; ++i) {
    cout << C[i] << " ";
  }
  cout << endl;
}
