//
// Created by debanjan on 8/3/23.
//

// Pseudocode
/*
 * Read two integers(noOfPeople,cutoffRank)
 * take an array with size 50
 *Now read score of all the people
 * Now find out how many people scored more or equal
 * marks than the cutoff rank holder(numbers must be +ve)
 * now return the number of people who'll advance to the next round
 * */

#include <stdio.h>

int main() {
  int noOfPeople, cutoffRank;
  int cutoffScore = 0;
  int scoreArray[50];
  int noOfPeopleInNextRound = 0;

  scanf("%d %d", &noOfPeople, &cutoffRank);

  for (int i = 0; i < noOfPeople; ++i) {
    scanf("%d", &scoreArray[i]);
  }
  cutoffScore = scoreArray[cutoffRank - 1];
  for (int i = 0; i < noOfPeople; ++i) {
    if (scoreArray[i] >= cutoffScore && scoreArray[i] > 0) {
      noOfPeopleInNextRound++;
    }
  }
  printf("%d", noOfPeopleInNextRound);
}