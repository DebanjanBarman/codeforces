//
// Created by debanjan on 8/6/23.
//
#include <cmath>
#include <iostream>
using namespace std;

int main() {
  int matrix[5][5];
  int one_row = 0, one_col = 0;

  // Read the input matrix
  for (int i = 0; i < 5; ++i) {

    for (int j = 0; j < 5; ++j) {
      cin >> matrix[i][j];

      // Position of '1'
      if (matrix[i][j] == 1) {
        one_row = i;
        one_col = j;
      }
    }
  }

  // Calculate the minimum number of moves
  // The center is position [2,2]

  //  To clarify, the Manhattan distance (sum of absolute differences) between
  //  two points (x1, y1) and (x2, y2) in a 2D plane is given by:
  //  Manhattan distance = |x2 - x1| + |y2 - y1|

  int moves = abs(one_row - 2) + abs(one_col - 2);

  cout << moves << endl;

  return 0;
}
