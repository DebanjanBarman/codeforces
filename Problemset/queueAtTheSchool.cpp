#include <iostream>
using namespace std;

int main() {
  int n, time;
  string student;

  cin >> n >> time >> student;

  while (time--) {
    for (int i = 0; i < n - 1; ++i) {
      // Iterate up to n - 2 to avoid accessing student[n]

      if (student[i] == 'B' && student[i + 1] == 'G') {
        swap(student[i], student[i + 1]);
        ++i; // Skip the next student since they have already been swapped
      }
    }
  }

  cout << student << endl;
}
