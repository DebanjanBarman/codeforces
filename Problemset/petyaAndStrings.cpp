//
// Created by debanjan on 8/6/23.
//
#include <bits/stdc++.h>
#include <iostream>
#include <string>

using namespace std;

int main() {
  string str1;
  string str2;

  cin >> str1;
  cin >> str2;

  transform(str1.begin(), str1.end(), str1.begin(), ::tolower);
  transform(str2.begin(), str2.end(), str2.begin(), ::tolower);

  int compareResult = str1.compare(str2);

  if (compareResult == 0) {
    cout << 0 << endl;
  } else if (compareResult > 0) {
    cout << 1 << endl;
  } else {
    cout << -1 << endl;
  }

  return 0;
}