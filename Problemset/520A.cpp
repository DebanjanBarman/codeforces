//
// Created by debanjan on 8/18/23.
//

#include <iostream>
#include <unordered_set>

using namespace std;
int main() {
  int n;
  cin >> n;
  string str = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

  unordered_set<char> uniqueChars;
  char inputStr[n];

  for (int i = 0; i < n; ++i) {
    char temp;
    cin >> temp;

    temp = toupper(temp);
    inputStr[i] = temp;
    uniqueChars.insert(temp);
  }
  if (uniqueChars.size() < 26) {
    cout << "NO\n";
    return 0;
  } else {
    for (char s : str) {
      if (uniqueChars.find(s) == uniqueChars.end()) {
        cout << "NO\n";
        return 0;
      }
    }
  }
  cout << "YES\n";
  return 0;
}