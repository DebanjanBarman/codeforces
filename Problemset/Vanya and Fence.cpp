//
// Created by debanjan on 8/13/23.
//
#include <iostream>
using namespace std;

int countWidth(int personHeight, int wallHeight) {
  if (personHeight > wallHeight) {
    return 2;
  }
  return 1;
}

int main() {
  int n, h, width = 0;

  scanf("%d %d", &n, &h);

  int arr[n];
  for (int i = 0; i < n; ++i) {
    cin >> arr[i];
  }
  for (int i = 0; i < n; ++i) {
    width += countWidth(arr[i], h);
  }

  cout << width << endl;
}