//
// Created by debanjan on 8/3/23.
//
/*
 * Pseudocode
 *
 * use a 2D array with size array[150][4], 4 because of '\0'
 * read the no of statements
 * use a for loop with max limit of the no of statements
 * if each of the string is X++ or X-- or --X or ++X
 * increase of decrease the value of X
 * print the final value
 * */
#include <stdio.h>
#include <string.h>

int main() {
  char statementArray[150][4];
  int noOfStatements = 0;
  int X = 0;

  scanf("%d", &noOfStatements);

  for (int i = 0; i < noOfStatements; ++i) {
    scanf("%s", statementArray[i]);
  }

  for (int i = 0; i < noOfStatements; ++i) {

    if (strcmp(statementArray[i], "X++") == 0) {
      X++;
    } else if (strcmp(statementArray[i], "++X") == 0) {
      X++;
    } else if (strcmp(statementArray[i], "--X") == 0) {
      X--;
    } else if (strcmp(statementArray[i], "X--") == 0) {
      X--;
    }
  }
  printf("%d", X);
}