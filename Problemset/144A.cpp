#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;

  int array[n];
  int lowest, highest;
  int indexOfLowest, indexOfHighest;

  for (int i = 0; i < n; ++i) {
    cin >> array[i];
  }
  lowest = array[0];
  indexOfLowest = 0;
  highest = array[0];
  indexOfHighest = 0;

  for (int i = 0; i < n; ++i) {
    if (array[i] > highest) {
      highest = array[i];
      indexOfHighest = i;
    }
    if (array[i] < lowest) {
      lowest = array[i];
      indexOfLowest = i;
    }
  }
  cout << abs(0 - indexOfHighest) + abs((n - 1) - indexOfLowest) << endl;
}
