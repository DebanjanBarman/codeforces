//
// Created by debanjan on 8/11/23.
//

#include <iostream>
using namespace std;

int main() {
  int n = {};
  string str = {};
  int noOfRepeatedChars = {};

  cin >> n;
  cin >> str;

  for (int i = 0; i < str.length(); ++i) {
    if (str[i] == str[i + 1]) {
      noOfRepeatedChars++;
    }
  }
  cout << noOfRepeatedChars << endl;
}