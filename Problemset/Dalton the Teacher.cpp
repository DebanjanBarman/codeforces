#include <iostream>
using namespace std;

void problem() {
  int n, correctPosition = 0;
  cin >> n;

  int array[n];
  for (int i = 0; i < n; ++i) {
    cin >> array[i];
  }
  for (int i = 0; i < n; ++i) {
    if (array[i] == i + 1) {
      correctPosition++;
    }
  }
  if (correctPosition % 2 == 0) {
    cout << correctPosition / 2 << endl;
  } else {
    cout << (correctPosition + 1) / 2 << endl;
  }
}

int main() {
  int n;
  cin >> n;
  while (n--) {
    problem();
  }
}