#include <iostream>
#include <string>

using namespace std;

int main() {
  string dislike = "I hate ";
  string like = "I love ";
  string c1 = "that ";
  string c2 = "it";

  int n;
  cin >> n;

  string result = "";
  for (int i = 1; i <= n; i++) {
    if (i % 2 == 0) {
      result += like;
    } else {
      result += dislike;
    }

    if (i < n) {
      result += c1;
    } else {
      result += c2;
    }
  }

  cout << result << endl;

  return 0;
}