#include <stdio.h>

int main() {
  int n = 0;
  scanf("%d", &n);
  if (n == 2) {
    printf("NO");
  } else if (n % 2 == 0) {
    printf("YES");
  } else {
    printf("NO");
  }
  return 0;
}