//
// Created by debanjan on 8/7/23.
//

/*PseudoCode
 * store the no in an array(each digit as an array element)
 * if the 0th index is >=5 then make the 0th index 1, and make rest of the
 * element 0, and increase the length of the number by adding a new 0 at the end
 *
 * else scan from the 0th index if any number is >=5 then increase the previous
 * index by 1 and make rest of the digits 0
 * */

#include <iostream>
#include <string>
using namespace std;

int main() {
  int noOfTests = 0;
  string number = {};

  cin >> noOfTests;

  for (int i = 0; i < noOfTests; ++i) {
    cin >> number;

    int lenOfNo = number.length();

    int roundedTimes = 20;

    while (roundedTimes > 0) {
      for (int j = 0; j < lenOfNo; ++j) {
        int noAsInt = (int(number[0]) - 48);

        if (noAsInt >= 5) {
          number[0] = '1';

          for (int k = 1; k < lenOfNo; ++k) {
            number[k] = '0';
          }
          number.push_back('0');
          continue;
        }

        int noAsInt2 = (int(number[j]) - 48);

        if (noAsInt2 >= 5) {
          int noToReplaceWith = (((int)number[j - 1] - 48) + 1);
          number[(j - 1)] = ((char)(noToReplaceWith + 48));

          for (int k = j; k < lenOfNo; ++k) {
            number[k] = '0';
          }
        }
      }
      roundedTimes--;
    }

    cout << number << endl;
  }

  return 0;
}