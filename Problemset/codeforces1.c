//
// Created by debanjan on 8/5/23.
//
/*
 * Pseudocode
 * read one integer(no of tests) max input is 500
 * then read one integer(the length of the array) range is [2,50]
 * then read all the elements of the array(each element can be from 1 to 10^9)
 * you can only perform subtraction of 1 from each element of the array until
 * the element is 0 or the array is sorted and keep track of the number of times
 * you perform the operation and print the number
 * */

#include <stdio.h>

int sorted(int lenOfArray, int array[50]);
int operation(int lenOfArray, int array[50]);

int main() {
  int noOfTests;
  int lenOfArray;
  int array[50];
  int noOfOperations = 0;

  scanf("%d", &noOfTests);

  for (int i = 0; i < noOfTests; ++i) {
    scanf("%d", &lenOfArray);

    for (int j = 0; j < lenOfArray; ++j) {
      scanf("%d", &array[j]);
    }

    while (!sorted(lenOfArray, array)) {
      operation(lenOfArray, array);
      noOfOperations++;
    }

    printf("%d\n", noOfOperations);
    noOfOperations = 0;
  }
  return 0;
}

int sorted(int lenOfArray, int array[50]) {
  int currentValue = 0;

  for (int i = 0; i < lenOfArray; ++i) {
    if (currentValue <= array[i]) {
      currentValue = array[i];
    } else {
      return 0;
    }
  }
  return 1;
}

int operation(int lenOfArray, int array[50]) {
  for (int i = 0; i < lenOfArray; ++i) {
    if (array[i] == 0) {
      continue;
    } else {
      array[i] -= 1;
    }
  }
  return 0;
}