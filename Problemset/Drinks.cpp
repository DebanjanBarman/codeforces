//
// Created by debanjan on 8/16/23.
//

#include <iostream>
using namespace std;
#define lli long long int

int main() {
  lli n;
  cin >> n;
  lli array[n];
  lli sumPercentage = 0;
  lli totalPercentage = n * 100;

  for (int i = 0; i < n; ++i) {
    cin >> array[i];
    sumPercentage += array[i];
  }
  cout << double((double(sumPercentage) / double(totalPercentage))) * 100
       << endl;
}