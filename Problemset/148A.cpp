
#include <iostream>
#include <unordered_set>
using namespace std;

int main() {
  int k, l, m, n, d;
  unordered_set<int> set;
  cin >> k >> l >> m >> n >> d;

  for (int i = 1; i <= d; ++i) {
    if (k > d && l > d && m > d && n > d) {
      continue;
    } else if (i % k == 0) {
      set.insert(i);
    } else if (i % l == 0) {
      set.insert(i);
    } else if (i % m == 0) {
      set.insert(i);
    } else if (i % n == 0) {
      set.insert(i);
    }
  }
  cout << set.size() << endl;
}