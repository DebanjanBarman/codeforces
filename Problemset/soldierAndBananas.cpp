//
// Created by debanjan on 8/11/23.
//
#include <iostream>
using namespace std;

int main() {
  int noOfBanana, priceOfFistBanana, moneyHeHas = {};
  int totalCost = {};

  cin >> priceOfFistBanana >> moneyHeHas >> noOfBanana;

  for (int i = 1; i <= noOfBanana; ++i) {
    totalCost += (priceOfFistBanana * i);
  }

  moneyHeHas < totalCost ? cout << totalCost - moneyHeHas << endl
                         : cout << 0 << endl;

  return 0;
}