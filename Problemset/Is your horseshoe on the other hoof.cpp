//
// Created by debanjan on 8/17/23.
//
#include <iostream>
#include <unordered_set>

using namespace std;
int main() {
  unordered_set<int> set;
  int n[4];
  for (int &i : n) {
    cin >> i;
    set.insert(i);
  }

  cout << 4 - set.size() << endl;
}