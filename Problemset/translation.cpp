#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int main() {
  string s = {};
  string t = {};

  cin >> s;
  cin >> t;

  reverse(t.begin(), t.end());

  int cmp = t.compare(s);

  cmp == 0 ? cout << "YES" << endl : cout << "NO" << endl;
}