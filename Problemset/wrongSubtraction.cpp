#include <iostream>
using namespace std;

int main() {
  int n, k, currentResult;
  cin >> n >> k;

  currentResult = n;

  while (k > 0) {
    if (currentResult >= 10) {
      if (currentResult % 10 == 0) {
        currentResult /= 10;
      } else {
        currentResult--;
      }
    } else {
      currentResult--;
    }
    k--;
  }

  cout << currentResult;
}