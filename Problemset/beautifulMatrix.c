//
// Created by debanjan on 8/4/23.
//
/*
 *
  0 0 0 0 0
  0 0 0 0 0
  0 1 0 0 0
  0 0 0 0 0
  0 0 0 0 0
 *
 * Pseudocode
 * first scan the 25 elements and store it in a 2D array[5][5] size
 * find the position of '1' in [1,2] format
 * if one of the position 2 then we can move '1' into [2,2] position with only 1
 move
 * otherwise use recursion/
 * increase the left position by 1 + increase the right position by one
 * if the position in [3,3] return 0
 * */

#include <stdio.h>

int moves(int x, int y);
int main() {
  int matrix[5][5];
  int position[2];
  int noOfMoves = 0;

  for (int i = 0; i < 5; ++i) {
    scanf("%d %d %d %d %d", &matrix[i][0], &matrix[i][1], &matrix[i][2],
          &matrix[i][3], &matrix[i][4]);
  }
  for (int i = 0; i < 5; ++i) {
    for (int j = 0; j < 5; ++j) {
      if (matrix[i][j] == 1) {
        position[0] = i;
        position[1] = j;
      }
    }
  }
  printf("Position of 1 is %d, %d", position[0], position[1]);
  noOfMoves = moves(position[0], position[1]);

  printf("No of moves is %d", noOfMoves);
  return 0;
}

int moves(int x, int y) {
  if (x == 2 && y == 2) {
    return 0;
  } else if (x == 2 && y == 1) {
    return 1;
  } else if (x == 1 && y == 2) {
    return 1;
  }
  return moves(x + 1, y) + moves(x, y + 1);
}
