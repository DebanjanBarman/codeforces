//
// Created by debanjan on 8/3/23.
//
// Pseudocode
// Read one integer from stdin
// Take a 2D array of size [1000][3]
// Use a for loop with max range of array[integer]
// now if at least two of the items from
// array[integer][0],array[integer][1],array[integer][2] are one,
// Then increase a temporary variable by one
// at last print the value of the temp variable

#include <stdio.h>

int main() {
  int noOfQuestions = 0;
  int implementedQuestions = 0;

  int questionArray[1000][3];
  scanf("%d", &noOfQuestions);

  for (int i = 0; i < noOfQuestions; ++i) {
    scanf("%d %d %d", &questionArray[i][0], &questionArray[i][1],
          &questionArray[i][2]);
  }
  for (int i = 0; i < noOfQuestions; ++i) {

    if (questionArray[i][0] == 1 && questionArray[i][1] == 1 &&
        questionArray[i][2] == 1) {
      implementedQuestions++;
    } else if (questionArray[i][0] == 1 && questionArray[i][1] == 1 &&
               questionArray[i][2] == 0) {
      implementedQuestions++;
    } else if (questionArray[i][0] == 1 && questionArray[i][1] == 0 &&
               questionArray[i][2] == 1) {
      implementedQuestions++;
    } else if (questionArray[i][0] == 0 && questionArray[i][1] == 1 &&
               questionArray[i][2] == 1) {
      implementedQuestions++;
    }
  }
  printf("%d", implementedQuestions);
}