#include <iostream>
#include <unordered_set>

using namespace std;
int main() {
  unordered_set<int> set;
  int level;
  cin >> level;

  int X;
  cin >> X;

  int XLevel[X];
  for (int i = 0; i < X; ++i) {
    cin >> XLevel[i];
    set.insert(XLevel[i]);
  }

  int Y;
  cin >> Y;
  int YLevel[Y];

  for (int i = 0; i < Y; ++i) {
    cin >> YLevel[i];
    set.insert(YLevel[i]);
  }

  for (int i = 0; i < level; ++i) {
    if (set.find(int(i + 1)) == set.end()) {
      cout << "Oh, my keyboard!\n";
      return 0;
    }
  }
  cout << "I become the guy.\n";
}