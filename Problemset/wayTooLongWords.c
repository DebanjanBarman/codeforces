#include <stdio.h>
#include <string.h>

int main() {
  char str_array[100][100];

  int inputSize = 0;
  scanf("%d", &inputSize);

  for (int i = 0; i < inputSize; i++) {
    scanf("%s", str_array[i]);
  }

  for (int i = 0; i < inputSize; i++) {
    int lenOfString = strlen(str_array[i]);
    if (lenOfString > 10 && lenOfString <= 100) {
      int wordCountInMiddle = lenOfString - 2;
      printf("%c%d%c\n", str_array[i][0], wordCountInMiddle,
             str_array[i][lenOfString - 1]);
    } else if (lenOfString > 100) {
      printf("%c%d%c\n", str_array[i][0], 98, str_array[i][99]);
    } else {
      printf("%s\n", str_array[i]);
    }
  }

  return 0;
}