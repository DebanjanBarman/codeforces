#include <iostream>
#include <unordered_map>
using namespace std;

int main() {
  string a, b, c;
  cin >> a >> b >> c;

  unordered_map<char, int> map1, map2;

  for (int i = 0; i < a.size(); ++i) {
    map1[a[i]] += 1;
  }

  for (int i = 0; i < b.size(); ++i) {
    map1[b[i]] += 1;
  }

  for (int i = 0; i < c.size(); ++i) {
    map2[c[i]] += 1;
  }

  if (map1 == map2) {
    cout << "YES\n";
  } else {
    cout << "NO\n";
  }

  return 0;
}
