//
// Created by debanjan on 8/3/23.
//
/*
 * Pseudocode
 * read the value of m and n
 * find the area of m * n,
 * divide it by the area of one domino(2),
 * convert the result into integer
 * print the result
 * */

#include <stdio.h>
int main() {
  int m, n;
  scanf("%d %d", &m, &n);
  printf("%d", (int)(m * n) / 2);
}