#include <iostream>
using namespace std;

int main() {
  string str;
  int n, anton = 0, danik = 0;
  cin >> n;
  cin >> str;

  for (char c : str) {
    if (int(c) == 65) {
      anton++;
    } else if (int(c) == 68) {
      danik++;
    }
  }
  if (anton > danik) {
    cout << "Anton";
  } else if (danik > anton) {
    cout << "Danik";
  } else {
    cout << "Friendship";
  }
  return 0;
}