#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n;
  cin >> n;

  vector<int> giftTo(n + 1); // Using 1-based indexing for simplicity
  vector<int> result(n + 1);

  for (int i = 1; i <= n; ++i) {
    cin >> giftTo[i];
  }

  for (int i = 1; i <= n; ++i) {
    int val = giftTo[i];
    result[val] = i;
  }
  for (int i = 1; i <= n; ++i) {
    cout << result[i] << " ";
  }
  cout << " ";
  return 0;
}
// https://codeforces.com/problemset/problem/136/A