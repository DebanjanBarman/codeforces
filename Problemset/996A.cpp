#include <iostream>
using namespace std;

#define lli long long int

int main() {
  lli n;
  cin >> n;
  lli temp = n;
  int noOfBills = 0;

  while (temp >= 1) {

    if (temp >= 100) {
      noOfBills += 1;
      temp = temp - 100;
    } else if (temp >= 20) {
      noOfBills += 1;
      temp = temp - 20;
    } else if (temp >= 10) {
      noOfBills += 1;
      temp = temp - 10;
    } else if (temp >= 5) {
      noOfBills += 1;
      temp = temp - 5;
    } else if (temp < 5) {
      noOfBills += 1;
      temp--;
    }
  }
  cout << noOfBills << endl;
}