//
// Created by debanjan on 8/7/23.
//

#include <iostream>
using namespace std;

int sumOfArray(int arr[], int len);

int main() {
  int noOfTests = 0;
  int lenOfArray = 0;
  int array[50] = {};

  cin >> noOfTests;

  for (int i = 0; i < noOfTests; ++i) {
    cin >> lenOfArray;

    for (int j = 0; j < lenOfArray; ++j) {
      cin >> array[j];
    }

    int sumOfArr = sumOfArray(array, lenOfArray);

    if (sumOfArr % 2 == 0) {
      cout << "YES\n";
    } else {
      cout << "NO\n";
    }
  }
}

int sumOfArray(int arr[], int len) {
  int sum = 0;
  for (int i = 0; i < len; ++i) {
    sum += arr[i];
  }
  return sum;
}