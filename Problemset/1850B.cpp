/*Pseudocode
 *
 * create a new array and store the values with one element added to the
 * array(the position of the item)
 *
 * loop through the array and delete the words/(ignore the words) with length >
 * 10; now loop through the array and find out the highest value
 * (length*quality) and return its position;
 *
 * */

#include <iostream>
using namespace std;
int main() {
  int n;
  cin >> n;

  while (n--) {
    int len;
    cin >> len;

    int array[len][3];
    for (int i = 0; i < len; ++i) {
      cin >> array[i][0];
      cin >> array[i][1];
      array[i][2] = i + 1;
    }

    int highestQuality = 0, position = 0;

    for (int i = 0; i < len; ++i) {
      if (array[i][0] <= 10) {
        int currentQuality = array[i][1];
        if (currentQuality > highestQuality) {
          highestQuality = currentQuality;
          position = array[i][2];
        }
      }
    }
    cout << position << endl;
  }
}