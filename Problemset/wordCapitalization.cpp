//
// Created by debanjan on 8/7/23.
//
#include <cctype>
#include <iostream>

using namespace std;

int main() {
  char sentence[1001] = {};
  cin >> sentence;

  char ch = toupper(sentence[0]);
  sentence[0] = ch;

  cout << sentence << endl;
  return 0;
}