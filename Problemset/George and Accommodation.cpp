//
// Created by debanjan on 8/13/23.
//

#include <iostream>
using namespace std;

int main() {
  int n;
  int arr[100][2];
  int roomForAlex = 0;

  cin >> n;
  for (int i = 0; i < n; ++i) {
    scanf("%d %d", &arr[i][0], &arr[i][1]);
  }
  for (int i = 0; i < n; ++i) {
    if (arr[i][1] - arr[i][0] >= 2) {
      roomForAlex++;
    }
  }
  cout << roomForAlex << endl;
}