//
// Created by debanjan on 8/21/23.
//
/*
Tetrahedron. Tetrahedron has 4 triangular faces.
Cube. Cube has 6 square faces.
Octahedron. Octahedron has 8 triangular faces.
Dodecahedron. Dodecahedron has 12 pentagonal faces.
Icosahedron. Icosahedron has 20 triangular faces
 * */

#include <iostream>
#include <unordered_map>

using namespace std;
int main() {
  int n;
  cin >> n;
  string str[n];
  unordered_map<string, int> map = {{"Tetrahedron", 4},
                                    {"Cube", 6},
                                    {"Octahedron", 8},
                                    {"Dodecahedron", 12},
                                    {"Icosahedron", 20}};
  for (int i = 0; i < n; ++i) {
    cin >> str[i];
  }

  int faces = 0;
  for (string s : str) {
    int value = map[s];
    faces += value;
  }
  cout << faces << endl;
}