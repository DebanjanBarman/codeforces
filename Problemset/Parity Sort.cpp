//
// Created by debanjan on 8/14/23.
//

#include <bits/stdc++.h>
#include <iostream>
#include <vector>
using namespace std;

void solve() {
  int n;
  cin >> n;

  int array[n];
  vector<int> arrayCopy;

  bool possible = true;

  for (int i = 0; i < n; ++i) {
    cin >> array[i];
    arrayCopy.push_back(array[i]);
  }

  if (n == 1) {
    cout << "YES\n";
    return;
  }
  sort(arrayCopy.begin(), arrayCopy.end());
  for (int i = 0; i < n; ++i) {
    if (array[i] % 2 != arrayCopy[i] % 2) {
      possible = false;
      break;
    } else {
      possible = true;
    }
  }
  possible ? cout << "YES\n" : cout << "NO\n";
}
int main() {
  int n;
  cin >> n;
  while (n--) {
    solve();
  }
}