/*Pseudocode
 filling = h + c;

 if filling >= bread
    layer:= bread + (bread -1)

 if filling < bread:
    layer := filling + (filling +1)

*/

#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;
  while (n--) {
    int bread, c, h;
    cin >> bread >> c >> h;
    int filling = h + c;
    int layer = 0;

    if (filling >= bread) {
      layer = bread + (bread - 1);
    }
    if (filling < bread) {
      layer = filling + (filling + 1);
    }
    cout << layer << endl;
  }
}