#include <bits/stdc++.h>
#include <iostream>
#include <string>

using namespace std;

int main() {
  string userName = {};
  int userNameLength = {};
  unordered_set<char> set;

  cin >> userName;

  userNameLength = userName.length();

  for (int i = 0; i < userNameLength; ++i) {
    set.insert(userName[i]);
  }

  int uniqueCharacters = set.size();

  uniqueCharacters % 2 == 0 ? cout << "CHAT WITH HER!\n"
                            : cout << "IGNORE HIM!\n";

  return 0;
}