#include <iostream>
using namespace std;

int main() {
  int n;
  cin >> n;
  int arr[n];

  for (int i = 0; i < n; ++i) {
    cin >> arr[i];
  }
  int first = arr[0];
  int low = arr[0];
  int high = arr[0];
  int goodContest = 0;

  for (int i = 1; i < n; ++i) {
    if (arr[i] < low) {
      low = arr[i];
      goodContest++;
    }
    if (arr[i] > high) {
      high = arr[i];
      goodContest++;
    }
  }
  cout<<goodContest<<endl;
}