//
// Created by debanjan on 8/6/23.
//

#include <iostream>
int largestElement(const int array[50], int k);
using namespace std;

int main() {
  int noOfTests = 0;
  int arraySize = 0;
  int array[50];

  cin >> noOfTests;

  for (int i = 0; i < noOfTests; ++i) {
    cin >> arraySize;
    for (int j = 0; j < arraySize; ++j) {
      cin >> array[j];
    }

    int noOfOperations = 0;

    for (int k = arraySize - 1; k > 0; --k) {
      if (array[k] >= array[k - 1]) {
        continue;
      } else {
        noOfOperations = largestElement(array, k - 1);
        break;
      }
    }
    cout << noOfOperations << endl;
  }

  return 0;
}

int largestElement(const int array[50], int k) {
  int largest = array[0];

  for (int i = 0; i <= k; ++i) {
    if (array[i] > largest) {
      largest = array[i];
    } else {
      continue;
    }
  }

  return largest;
}
