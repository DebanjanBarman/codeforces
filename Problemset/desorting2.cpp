//
// Created by debanjan on 8/14/23.
//
#include <iostream>
using namespace std;
void solve();
bool isSorted(const long long int array[], long long int arraySize);

int main() {
  long long int times;
  cin >> times;
  while (times--) {
    solve();
  }
  return 0;
}

void solve() {
  long long int n;
  cin >> n;
  long long int array[n];

  for (long long int i = 0; i < n; ++i) {
    cin >> array[i];
  }

  long long int sizeOfArray = n;
  long long int minNoOfOps = 0;
  long long int currNoOfOps;

  if (!isSorted(array, sizeOfArray)) {
    cout << 0 << endl;
  } else {
    for (int i = 0; i < sizeOfArray - 1; ++i) {
      currNoOfOps = ((array[i + 1] - array[i]) / 2) + 1;

      if (i == 0) {
        minNoOfOps = currNoOfOps;
      } else if (currNoOfOps < minNoOfOps) {
        minNoOfOps = currNoOfOps;
      }
    }
    cout << minNoOfOps << endl;
  }
  return;
}

bool isSorted(const long long int array[], long long int arraySize) {
  bool sorted = true;
  for (long long int i = 0; i < arraySize - 1; ++i) {
    if (array[i] > array[i + 1]) {
      sorted = false;
    }
  }
  return sorted;
}
