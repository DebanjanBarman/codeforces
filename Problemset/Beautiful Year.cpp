//
// Created by debanjan on 8/13/23.
//
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int uniqueDigits(int num) {
  vector<int> array;
  int currentNum = num;
  while (currentNum > 0) {
    int digit = currentNum % 10;

    if (std::count(array.begin(), array.end(), digit)) {
      return false;
    } else {
      array.push_back(digit);
      currentNum /= 10;
    }
  }
  return true;
}

int main() {
  int year, currentYear;
  scanf("%d", &year);

  currentYear = year + 1;

  while (!uniqueDigits(currentYear)) {
    currentYear++;
  }
  cout << currentYear << endl;
  return 0;
}