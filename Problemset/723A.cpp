#include <bits/stdc++.h>
#include <iostream>
#include <vector>
using namespace std;
int main() {
  vector<int> arr;
  for (int i = 0; i < 3; ++i) {
    int n;
    cin >> n;
    arr.push_back(n);
  }
  sort(arr.begin(), arr.end());

  cout << (arr[2] - arr[1]) + (arr[1] - arr[0]) << endl;
}