//
// Created by debanjan on 8/13/23.
//

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main() {
  int n;
  cin >> n;
  vector<int> array;
  for (int i = 0; i < n; ++i) {
    int value;
    cin >> value;
    array.push_back(value);
  }
  if (std::count(array.begin(), array.end(), 1)) {
    cout << "HARD" << endl;
  } else {
    cout << "EASY" << endl;
  }
  return 0;
}