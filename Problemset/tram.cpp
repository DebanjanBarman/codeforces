//
// Created by debanjan on 8/13/23.
//
#include <iostream>
using namespace std;

int main() {
  int n;
  int maxCapacity = 0;
  int currentCapacity = 0;
  int data[1000][2] = {};

  cin >> n;
  for (int i = 0; i < n; ++i) {
    scanf("%d %d", &data[i][0], &data[i][1]);
  }

  for (int i = 0; i < n - 1; ++i) {
    if (data[i][0] < data[i][1]) {
      // increases
      int addedPassenger = data[i][1] - data[i][0];
      currentCapacity += addedPassenger;

    } else {
      // decreases
      int lostPassenger = data[i][0] - data[i][1];
      currentCapacity -= lostPassenger;
    }
    if (currentCapacity > maxCapacity) {
      maxCapacity = currentCapacity;
    }
  }

  cout << maxCapacity << endl;
  return 0;
}
