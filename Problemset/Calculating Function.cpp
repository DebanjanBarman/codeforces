#include <iostream>

#define lli long long int

using namespace std;
int main() {
  lli n = 0;
  lli sum = 0;

  cin >> n;

  if (n % 2 == 0) {
    sum = n / 2;
  } else {
    sum = -((n + 1) / 2);
  }

  cout << sum << endl;
}
