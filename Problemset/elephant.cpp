
#include <iostream>

using namespace std;

int main() {
  int distance, currentDistance;
  int steps = 0;

  cin >> distance;
  currentDistance = distance;

  while (currentDistance > 0) {
    if (currentDistance >= 5) {
      currentDistance = (currentDistance - 5);
      steps++;
    } else {
      steps++;
      currentDistance = 0;
    }
  }

  cout << steps << endl;
}