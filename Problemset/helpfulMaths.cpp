//
// Created by debanjan on 8/6/23.
//

#include <bits/stdc++.h>
#include <iostream>
#include <string>

int main() {
  std::string expression = {};
  int array[100] = {};
  int index = 0;
  std::cin >> expression;

  for (int i = 0; i < expression.length(); ++i) {
    if (i % 2 == 0) {
      char number = expression[i];
      array[index] = std::atoi(&number);
      index++;
    }
  }

  std::sort(array, array + index);

  for (int i = 0; i < index; ++i) {
    std::cout << array[i];
    if (i < index - 1) {
      std::cout << "+";
    }
  }
  return 0;
}